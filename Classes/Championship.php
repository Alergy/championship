<?php
/**
 * Created by PhpStorm.
 * User: Alergy
 * Date: 21.10.2017
 * Time: 2:17
 */
if (!defined('CHAMPIONSHIP_DATA_FILE_PATH')) {
    define('CHAMPIONSHIP_DATA_FILE_PATH', __DIR__ . '/../uploads/data.csv');
}

/** Championship root directory */
if (!defined('CHAMPIONSHIP_ROOT')) {
    define('CHAMPIONSHIP_ROOT', dirname(__FILE__) . '/');
    require(CHAMPIONSHIP_ROOT . 'SoccerTeam.php');
    require(CHAMPIONSHIP_ROOT . 'SoccerGame.php');
}

if (file_exists(__DIR__ . '/../include/phpexcel/Classes/PHPExcel.php'))
    require_once __DIR__ . '/../include/phpexcel/Classes/PHPExcel.php';


/**
 * Class Championship
 */
class Championship
{
    private $soccerTeams;

    public $oneSixteenth;
    public $oneEighth;
    public $oneQuarter;
    public $oneSecond;
    public $theFinal;

    public function initData()
    {
        if (!file_exists(CHAMPIONSHIP_DATA_FILE_PATH)) {
            return false;
        }

        $file_type = PHPExcel_IOFactory::identify(CHAMPIONSHIP_DATA_FILE_PATH);
        $objReader = PHPExcel_IOFactory::createReader($file_type)->setDelimiter(';');
        $objPHPExcel = $objReader->load(CHAMPIONSHIP_DATA_FILE_PATH);
        $result = $objPHPExcel->getActiveSheet()->toArray();

        $countTeams = count($result);
        //не хватает команд
        if ($countTeams < 33)
            return false;

        for ($i = 1; $i < $countTeams; $i++) {
            $this->soccerTeams[] = new SoccerTeam(
                $result[$i][0],
                $result[$i][1],
                $result[$i][1],
                $result[$i][3],
                $result[$i][4],
                $result[$i][5]
            );
        }
        return true;
    }

    public function drawingProcedure()
    {
        shuffle($this->soccerTeams);
        $this->oneSixteenth = $this->prepareStage($this->soccerTeams);
    }

    /**
     * подготовить этап
     * @param $teams
     * @return array
     */
    private function prepareStage($teams)
    {
        $result = [];
        $countGames = count($teams) / 2;
        for ($i = 0; $i < $countGames; $i++) {
            $result[] = new SoccerGame($teams[$i], $teams[$i + $countGames]);
        }
        return $result;
    }

    private function runStage($games)
    {
        $result = [];
        /**@var SoccerGame $game */
        foreach ($games as $game) {
            $result[] = $game->calculateWinner();
        }
        return $result;

    }


    public function run()
    {
        // одна шестнадцатая
        $result = $this->runStage($this->oneSixteenth);
        $this->oneEighth = $this->prepareStage($result);

        // одна восьмая
        $result = $this->runStage($this->oneEighth);
        $this->oneQuarter = $this->prepareStage($result);

        // одна четвертая
        $result = $this->runStage($this->oneQuarter);
        $this->oneSecond = $this->prepareStage($result);

        // полуфинал
        $result = $this->runStage($this->oneSecond);
        $this->theFinal = $this->prepareStage($result);

        // финал
        $result = $this->runStage($this->theFinal);

        return $result;
    }

    public function __construct()
    {
    }

    public function __toString()
    {
        $result = "<h2>1/16</h2>";
        $result .= "<div class=\"row\"><div class=\"col-md-3\">" . implode("</div><div class=\"col-md-3\">", $this->oneSixteenth) . "</div></div>";
        $result .= "<h2>1/8</h2>";
        $result .= "<div class=\"row\"><div class=\"col-md-3\">" . implode("</div><div class=\"col-md-3\">", $this->oneEighth) . "</div></div>";
        $result .= "<h2>1/4</h2>";
        $result .= "<div class=\"row\"><div class=\"col-md-3\">" . implode("</div><div class=\"col-md-3\">", $this->oneQuarter) . "</div></div>";
        $result .= "<h2>1/2</h2>";
        $result .= "<div class=\"row\"><div class=\"col-md-3  col-md-offset-3\">" . implode("</div><div class=\"col-md-3\">", $this->oneSecond) . "</div></div>";
        $result .= "<h2>Финал</h2>";
        $result .= "<div class=\"row\"><div class=\"col-md-4  col-md-offset-4\">" . implode("</div><div class=\"col-md-3\">", $this->theFinal) . "</div></div>";
        return $result;
    }

}