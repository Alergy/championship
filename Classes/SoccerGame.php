<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21.10.2017
 * Time: 3:58
 */

/**
 * Class SoccerGame
 */
class SoccerGame
{
    /**
     * @var SoccerTeam 1 команда
     */
    private $firstTeam;

    /**
     * @return SoccerTeam
     */
    public function getFirstTeam()
    {
        return $this->firstTeam;
    }

    /**
     * @return SoccerTeam
     */
    public function getSecondTeam()
    {
        return $this->secondTeam;
    }

    /**
     * @return SoccerTeam
     */
    public function getFirstTeamGoal()
    {
        return $this->firstTeamGoal;
    }

    /**
     * @return SoccerTeam
     */
    public function getSecondTeamGoal()
    {
        return $this->secondTeamGoal;
    }

    /**
     * @return SoccerTeam
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * @var SoccerTeam 2 команда
     */
    private $secondTeam;

    /**
     * @var SoccerTeam забито первой командой
     */
    private $firstTeamGoal;

    /**
     * @var SoccerTeam забито второй командой
     */
    private $secondTeamGoal;

    /**
     * @var SoccerTeam победитель
     */
    private $winner;

    public function __construct($firstTeam, $secondTeam)
    {
        $this->firstTeam = $firstTeam;
        $this->firstTeamGoal = 0;
        $this->secondTeam = $secondTeam;
        $this->secondTeamGoal = 0;
        $this->winner = null;
    }

    /**
     * провести матч и получить победителя
     * @return SoccerTeam
     */
    public function calculateWinner(){

        /**@var SoccerTeam $t1*/
        $t1 = $this->firstTeam;
        /**@var SoccerTeam $t2*/
        $t2 = $this->secondTeam;

        //среднее количество забитых голов за игру
        $powerA1 = $t1->getScoredGoals()/$t1->getGames();
        $powerA2 = $t2->getScoredGoals()/$t2->getGames();

        //среднее количество пропущенных голов за игру
        $powerD1 = $t1->getMissedGoals()/$t1->getGames();
        $powerD2 = $t2->getMissedGoals()/$t2->getGames();

        //я конечно не прогнозист но пусть удет еще коэфициент побед к поражениям
        $k1 = $t1->getVictory()/$t1->getDefeat();
        $k2 = $t2->getVictory()/$t2->getDefeat();

        //количество голов каждой команды рандомное число от 0 до
        //суммы среднего количества голов забитых и среднего кооличества голов пропущенных противником
        // умноженное на коэфициент побед и поражений
        $gool1 = rand(0, ($powerA1 + $powerD2)*$k1);
        $gool2 = rand(0, ($powerA2 + $powerD1)+$k2);

        //и судья не зафиксирует ничьей)
        if ($gool1 == $gool2){
            if (rand(0,1)>0)
                $gool1 +=1;
            else
                $gool2 +=1;
        }

        //записываем счет в игру
        $this->firstTeamGoal = $gool1;
        $this->secondTeamGoal = $gool2;
        //записываем достижения в комманду для дальнейшего расчета
        $this->firstTeam->sendResult($gool1, $gool2);
        $this->secondTeam->sendResult($gool2, $gool1);

        if($gool1 > $gool2)
            $this->winner = $this->firstTeam;
        else
            $this->winner = $this->secondTeam;

        return $this->winner;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $result = "<div class='thumbnail'><div class='row'>
            <div class='col-md-6'><p class='team'>".$this->firstTeam."</p><p class='count_game'>".$this->firstTeamGoal."</p></div>
            <div class='col-md-6'><p class='team'>".$this->secondTeam."</p><p class='count_game'>".$this->secondTeamGoal."</p></div>
            <div class='col-md-12'><h4>".$this->winner."</h4></div>
         </div></div>";

        return $result;
    }

}