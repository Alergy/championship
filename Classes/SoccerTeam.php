<?php
/**
 * Created by PhpStorm.
 * User: Alergy
 * Date: 21.10.2017
 * Time: 2:19
 */

/**
 * Class SoccerTeam
 */
class SoccerTeam
{
    /**
     * @var string Название команды
     */
    private $name;

    /**
     * @var integer игр проведено
     */
    private $games;

    /**
     * @var integer победы
     */
    private $victory;

    /**
     * @var integer ничьи
     */
    private $draws;

    /**
     * @var integer поражения
     */
    private $defeat;

    /**
     * @var integer забитые голы
     */
    private $scoredGoals;

    /**
     * @var integer пропущенные голы
     */
    private $missedGoals;

    /**
     * @return int
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * @return int
     */
    public function getVictory()
    {
        return $this->victory;
    }

    /**
     * @return int
     */
    public function getDraws()
    {
        return $this->draws;
    }

    /**
     * @return int
     */
    public function getDefeat()
    {
        return $this->defeat;
    }

    /**
     * @return int
     */
    public function getScoredGoals()
    {
        return $this->scoredGoals;
    }

    /**
     * @return int
     */
    public function getMissedGoals()
    {
        return $this->missedGoals;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $scoredGoals integer забито голов
     * @param $missedGoals integer пропущено голов
     */
    public function sendResult($scoredGoals,$missedGoals){
        if($scoredGoals == $missedGoals)
            $this->draws += 1;
        elseif ($scoredGoals > $missedGoals)
            $this->victory += 1;
        else
            $this->defeat += 1;
        $this->scoredGoals += $scoredGoals;
        $this->missedGoals += $missedGoals;
        $this->games += 1;
    }

    /**
     * SoccerTeam constructor.
     * @param $name
     * @param $games
     * @param $victory
     * @param $draws
     * @param $defeat
     * @param $goals
     */
    public function __construct($name, $games, $victory, $draws, $defeat, $goals)
    {
        $goals = str_replace(" ","",$goals);
        $goals = explode("-", $goals);
        $this->name = $name;
        $this->games = $games;
        $this->victory = $victory;
        $this->draws = $draws;
        $this->defeat = $defeat;
        $this->scoredGoals = $goals[0];
        $this->missedGoals = $goals[1];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}