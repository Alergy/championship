<?php
if (file_exists(__DIR__ . '/Classes/Championship.php'))
    require_once __DIR__ . '/Classes/Championship.php';
if (file_exists(__DIR__ . '/include/Services_JSON.php'))
    require_once __DIR__ . '/include/Services_JSON.php';

$championship = new Championship();
//прочитать данные
$championship->initData();
//жеребьевка
$championship->drawingProcedure();
//старт
$championship->run();

$result = ['championship' => $championship.""];
$objectJson = new Services_JSON();
echo $objectJson->encode($result);